package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Zone;

public interface ZoneService {
    Iterable<Zone> listAllZones();

    Zone getZoneById(Integer id);

    Zone saveZone(Zone comment);

    void deleteZone(Integer id);

}
